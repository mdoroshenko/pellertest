//
//  Question.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 24/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

enum Question {
    case first
    case second
    case third
    
    var string: String {
        switch self {
        case .first:
            return "Hello, how are you today?"
        case .second:
            return "How's going?"
        case .third:
            return "Give me my money"
        }
    }
    
    static var all: [Question] {
        return [.first, .second, .third]
    }
}
