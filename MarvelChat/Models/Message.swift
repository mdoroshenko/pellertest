//
//  Message.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 24/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

enum Message {
    case character(text: String)
    case user(text: String)
}
