//
//  ImagesCache.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 24/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

final class ImagesCache {
    static let shared = ImagesCache()
    
    var images: [String: Data] = [:]
}
