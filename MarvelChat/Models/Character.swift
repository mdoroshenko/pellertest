//
//  Character.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 23/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

struct CharacterResponse: Decodable {
    let data: CharacterData
}

struct CharacterData: Decodable {
    let offset: Int
    let total: Int
    let count: Int
    let results: [Character]
}

struct Character: Decodable {
    let id: Int
    let name: String
    let thumbnail: CharacterThumbnail
}

struct CharacterThumbnail: Decodable {
    let path: String
    let ext: String
    
    var fullPath: String {
        return path + "." + ext
    }
    
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}

extension Character {
    // Possible responses for questions
    static func response(for question: Question) -> String {
        switch question {
        case .first:
            return "I'm fine"
        case .second:
            return "Okay"
        case .third:
            return "Sorry, no money"
        }
    }
}


