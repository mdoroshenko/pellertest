//
//  CharactersNetworkClient.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 23/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

final class CharactersNetworkClient: CharactersClient {
    enum Errors: Error {
        case network
        case decoding
    }
    
    private let privateKey = "30e1843cd0a198e978eb2d708829393a16de7231"
    private let publicKey = "4eca74560a98aa438f9179767e35cb63"
    private let limit = 20
    private let session = URLSession.shared
    
    private func url(for page: Int) -> URL {
        let timestamp = String(Int(Date().timeIntervalSinceReferenceDate))
        let hash = MD5(string: timestamp + privateKey + publicKey).map { String(format: "%02hhx", $0) }.joined()
        let urlString = "https://gateway.marvel.com:443/v1/public/characters?apikey=\(publicKey)&limit=\(limit)&offset=\(limit * page)&ts=\(timestamp)&hash=\(hash)"
        return URL(string: urlString)!
    }
    
    func MD5(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
    func fetch(page: Int, completion: @escaping (Result<CharacterData, Error>) -> Void) {
        session.dataTask(with: url(for: page)) { (data, response, error) in
            guard error == nil else {
                completion(.failure(error!))
                return
            }
            guard let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = data else {
                    completion(.failure(Errors.network))
                    return
            }
            guard let decoding = try? JSONDecoder().decode(CharacterResponse.self, from: data) else {
                completion(.failure(Errors.decoding))
                return
            }
            completion(.success(decoding.data))
        }.resume()
    }
}
