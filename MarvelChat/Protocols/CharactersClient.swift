//
//  CharactersClient.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 23/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

protocol CharactersClient {
    func fetch(page: Int, completion: @escaping (Result<CharacterData, Error>) -> Void)
}
