//
//  ChatViewController.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 23/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import UIKit

final class ChatViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var questionsContraint: NSLayoutConstraint!
    
    private var viewModel: ChatViewModel!
    var character: Character?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ChatViewModel(characterName: character?.name ?? "", characterId: character?.id ?? 0, delegate: self)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                bottomConstraint.constant = 0
                questionsContraint.constant = 50
            } else {
                bottomConstraint.constant = (endFrame?.size.height ?? 0)  + 32
                questionsContraint.constant = 8
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
}

extension ChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.messagesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = viewModel.message(at: indexPath.row)
        let identifier: String
        let text: String
        switch message {
        case .character(let t):
            identifier = "character"
            text = t
        case .user(let t):
            identifier = "user"
            text = t
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        cell.textLabel?.text = text
        return cell
    }
}

extension ChatViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.questionsСount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuestionCollectionViewCell
        cell.config(text: viewModel.question(at: indexPath.row).string)
        return cell
    }
}

extension ChatViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = viewModel.question(at: indexPath.row).string
        let width = text.size(withAttributes:[.font: UIFont.systemFont(ofSize:17.0)]).width
        return CGSize(width: width + 8, height: 44)
    }
}

extension ChatViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        textField.resignFirstResponder()
        viewModel.ask(question: viewModel.question(at: indexPath.row))
    }
}

extension ChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text {
            viewModel.ask(text: text)
            textField.text = nil
        }
        return true
    }
}

extension ChatViewController: ChatViewModelDelegate {
    func didUpdate() {
        // TODO: insert new messages
        tableView.reloadData()
        let last = viewModel.messagesCount - 1
        if last >= 0 {
            tableView.scrollToRow(at: IndexPath(row: last, section: 0), at: .bottom, animated: true)
        }
    }
}
