//
//  CharactersViewController.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 23/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import UIKit

final class CharactersViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel: CharactersViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CharactersViewModel(delegate: self)
        viewModel.fetch()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let index = tableView.indexPathForSelectedRow?.row, let vc = segue.destination as? ChatViewController {
            // TODO: name may be async
            vc.character = viewModel.character(at: index)
        }
    }
}

extension CharactersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CharacterTableViewCell
        let character = viewModel.character(at: indexPath.row)
        cell.config(name: character?.name ?? "", path: character?.thumbnail.fullPath ?? "")
        return cell
    }
}

extension CharactersViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: { $0.row > viewModel.currentCount }) {
            viewModel.fetch()
        }
    }
}

extension CharactersViewController: CharactersViewModelDelegate {
    func onFetchFirstComplete() {
        tableView.reloadData()
    }
    
    func onFetchNextComplete(indexPaths: [IndexPath]) {
        tableView.reloadRows(at: indexPaths, with: .none)
    }
    
    func onFetchFault() {
        // TODO: handle
    }
}
