//
//  CharacterTableViewCell.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 24/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import UIKit

final class CharacterTableViewCell: UITableViewCell {
    @IBOutlet weak var characterImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        characterImageView.image = nil
    }
    
    func config(name: String, path: String) {
        guard !name.isEmpty || !path.isEmpty else { return }
        nameLabel.text = name
        if let data = ImagesCache.shared.images[path] {
            characterImageView.image = UIImage(data: data)
        } else {
            DispatchQueue.global().async { [weak self] in
                guard let url = URL(string: path) else { return }
                guard let data = try? Data(contentsOf: url) else { return }
                guard let image = UIImage(data: data) else { return }
                DispatchQueue.main.async {
                    ImagesCache.shared.images[path] = data
                    self?.characterImageView.image = image
                }
            }
        }
    }
}
