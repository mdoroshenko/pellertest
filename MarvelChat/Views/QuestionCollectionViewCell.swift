//
//  QuestionCollectionViewCell.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 24/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import UIKit

final class QuestionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var textLabel: UILabel!
    
    func config(text: String) {
        textLabel.text = text
    }
}
