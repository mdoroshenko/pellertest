//
//  CharactersViewModel.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 23/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

protocol CharactersViewModelDelegate: class {
    func onFetchFirstComplete()
    func onFetchNextComplete(indexPaths: [IndexPath])
    func onFetchFault()
}

final class CharactersViewModel {
    private weak var delegate: CharactersViewModelDelegate?
    
    private var characters: [Character] = []
    private var firstFetchComplete = false
    private var isFetchInProgress = false
    private var total = 0
    private var nextPage = 0
    
    private let client: CharactersClient = CharactersNetworkClient()
    
    var count: Int {
        return total
    }
    
    var currentCount: Int {
        return characters.count
    }
    
    init(delegate: CharactersViewModelDelegate) {
        self.delegate = delegate
    }
    
    func character(at index: Int) -> Character? {
        guard index < characters.count else { return nil }
        return characters[index]
    }
    
    func fetch() {
        guard !isFetchInProgress else { return }
        guard !firstFetchComplete || currentCount != count else { return }
        isFetchInProgress = true
        client.fetch(page: nextPage) { [weak self] (result) in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.isFetchInProgress = false
                switch result {
                case .success(let data):
                    if !self.firstFetchComplete {
                        self.firstFetchComplete = true
                        self.characters = data.results
                        self.total = data.total
                        self.delegate?.onFetchFirstComplete()
                    } else {
                        let startIndex = self.characters.count
                        self.characters = self.characters + data.results
                        let endIndex = self.characters.count - 1
                        self.delegate?.onFetchNextComplete(indexPaths: (startIndex...endIndex).map { IndexPath(row: $0, section: 0) })
                    }
                    self.nextPage += 1
                case .failure(_):
                    // TODO: handle error
                    self.delegate?.onFetchFault()
                }
            }
        }
    }
}
