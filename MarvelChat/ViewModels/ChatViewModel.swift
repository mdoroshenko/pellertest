//
//  ChatViewModel.swift
//  MarvelChat
//
//  Created by Max Doroshenko on 24/07/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import UIKit
import CoreData

protocol ChatViewModelDelegate: class {
    func didUpdate()
}

final class ChatViewModel {
    enum Constants {
        enum CoreData {
            static let chatKey = "Chat"
        }
    }
    
    private weak var delegate: ChatViewModelDelegate?
    
    private let questions = Question.all
    private var characterId = 0
    private var messages: [Message] = [] {
        didSet {
            save()
        }
    }
    
    var messagesCount: Int {
        return messages.count
    }
    
    var questionsСount: Int {
        return questions.count
    }
    
    init(characterName: String, characterId: Int, delegate: ChatViewModelDelegate) {
        self.characterId = characterId
        load()
        if messages.count == 0 {
            messages.insert(.character(text: "привет, я \(characterName)"), at: 0)
        }
        self.delegate = delegate
    }
    
    func message(at index: Int) -> Message {
        return messages[index]
    }
    
    func question(at index: Int) -> Question {
        return questions[index]
    }
    
    func ask(question: Question) {
        messages.append(.user(text: question.string))
        makeResponse(for: question)
        delegate?.didUpdate()
    }
    
    func ask(text: String) {
        messages.append(.user(text: text))
        makeResponse(for: nil)
        delegate?.didUpdate()
    }
    
    private func makeResponse(for question: Question?) {
        if let question = question {
            messages.append(.character(text: Character.response(for: question)))
        } else {
            messages.append(.character(text: "I don't understand"))
        }
    }
    
    // MARK: CoreData
    private var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    private var context: NSManagedObjectContext {
        return appDelegate.persistentContainer.viewContext
    }
    
    private var fetchRequest: NSFetchRequest<Chat> {
        let fetchRequest =
            NSFetchRequest<Chat>(entityName: Constants.CoreData.chatKey)
        fetchRequest.predicate = NSPredicate(format: "characterId = %d", characterId)
        return fetchRequest
    }
    
    private func save() {
        // TODO: optimize
        do {
            let chats = try context.fetch(fetchRequest)
            let chat: Chat
            if let first = chats.first {
                chat = first
            } else {
                chat = Chat(context: context)
                chat.characterId = Int32(characterId)
            }
            let messages: [ChatMessage] = self.messages.map {
                let chatMessage = ChatMessage(context: context)
                let text: String
                let isUser: Bool
                switch $0 {
                case .character(let t):
                    text = t
                    isUser = false
                case .user(let t):
                    text = t
                    isUser = true
                }
                chatMessage.text = text
                chatMessage.isUser = isUser
                chatMessage.chat = chat
                return chatMessage
            }
            chat.messages = NSOrderedSet(array: messages)
            appDelegate.saveContext()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    private func load() {
        do {
            let chats = try context.fetch(fetchRequest)
            if let messages = chats.first?.messages?.array as? [ChatMessage] {
                self.messages = messages.map {
                    if $0.isUser {
                        return .user(text: $0.text ?? "")
                    } else {
                        return .character(text: $0.text ?? "")
                    }
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
